"""
Characters

Characters are (by default) Objects setup to be puppeted by Accounts.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
from evennia.contrib.rpg.rpsystem import ContribRPCharacter
from evennia.contrib.game_systems.clothing import ClothedCharacter

# rpsystem
class Character(ContribRPCharacter, ClothedCharacter):
    pass
